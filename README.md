# Unreal Project Template
Unreal Engine project template catering to the common use cases at RWTH VR. 
Contains:
- Configuration files for our CI
- Unreal configuration files for CAVE usage
- Unreal configuration files for sane HMD defaults

## Contents

- `Source` only contains a basic module source file such that Unreal recognizes this as a C++ project.
- `Content` contains a simple Template map
- `Config` contains the configuration parts, which make out the important aspect of this template:
  - `DefaultScalability.ini` sets sane Scalability settings which improve performance in VR
  - `DefaultGame.ini` sets a few Game settings for LiveLink and Multiuser usage
  - `Linux/Windows` settings in the corresponding folders set the respective nDisplay Engines for cluster usage. On Linux, `r.DefaultBackBufferPixelFormat = 1` is set for correct color values on the Cave in cluster mode.

## Getting Started

**For a more detailed description, see the [Getting Started Wiki](https://git-ce.rwth-aachen.de/vr-vis/unreal-wiki/-/wikis/Unreal/Getting-Started/Unreal-5-Template)**


## FAQ
Visit the [group wiki](https://git-ce.rwth-aachen.de/vr-vis/group-wiki/-/wikis/Unreal/Unreal-Index) for more information.
